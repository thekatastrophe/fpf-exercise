#!flask/bin/python

from flask import Flask, jsonify, abort, request
import time, datetime

app = Flask(__name__)

@app.route('/')
def index():
	return "This service provides a REST API for tracking GitHub pull requests. Please configure your GitHub repository to call the webhook available at `/api/v1.0/pr` with a JSON payload."

# Route the POST request
@app.route('/api/v1.0/pr', methods=['POST'])
def post_pull_request():
	# Make sure the request is coming in as JSON.
	if not request.json:
		abort(400)

	# Convert the Github-supplied timestamp to a Unix timestamp - use updated_at so it catches the right time for open/close events.
	# Incoming TS format: 2018-10-15T03:27:23Z
	timestamp = time.mktime(datetime.datetime.strptime(request.json['pull_request']['updated_at'], "%Y-%m-%dT%H:%M:%SZ").timetuple())

	# Prepare a line of CSV data to be appended to the data file.
	csvdata = str(request.json['pull_request']['number']) + ',"' + request.json['pull_request']['user']['login'] + '","' + str(timestamp) + '","' + request.json['pull_request']['state'] + '","' + request.json['pull_request']['head']['ref'] + '"' + "\n"

	# Open the datafile and write the line of CSV
	# (fail with 500 if there's a permission error or similar)
	datafile = open("data/pull_requests.csv", "a") or abort(500)
	datafile.write(csvdata) or abort(500)
	datafile.close()

	# Return success if we've managed it.
	return jsonify({'success': "true"})

if __name__ == '__main__':
	app.run(debug=True)